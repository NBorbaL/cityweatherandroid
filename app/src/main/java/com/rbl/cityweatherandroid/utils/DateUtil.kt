package com.rbl.cityweatherandroid.utils

import android.annotation.SuppressLint
import android.text.format.DateUtils
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DateUtil {
    companion object {
        private const val DEFAULT_DATE_FORMAT = "yyyy-MM-dd"

        /**
         * Converts string supplied in yyyy-MM-dd format to Date
         *
         * @param dateString Date in yyyy-MM-dd
         * @return Date object
         */
        @SuppressLint("SimpleDateFormat")
        fun convertStringToDate(dateString: String?): Date? {
            if (!dateString.isNullOrEmpty()) {
                val sdf = SimpleDateFormat(DEFAULT_DATE_FORMAT)
                try {
                    return sdf.parse(dateString)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
            }
            return null
        }

        /**
         * Converts a date object to a formatted string based on the default locale
         *
         * @param date Date to convert
         * @return Formatted date string
         */
        fun dateToLocalFormatString(date: Date?): String? {
            date?.let {
                return DateFormat.getDateInstance().format(date)
            }
            return null
        }

        /**
         *  Returns day of week based on date
         *
         * @param date Date
         * @param shortName true = short week name | false = long week name
         * @return Day of week string
         */
        fun getDayOfWeek(date: Date?, shortName: Boolean = false): String? {
            date?.let {
                val cal = Calendar.getInstance()
                cal.time = date
                return cal.getDisplayName(
                    Calendar.DAY_OF_WEEK,
                    if (shortName) Calendar.SHORT else Calendar.LONG,
                    Locale.getDefault()
                )
            }
            return null
        }

        /**
         * Gets relative string for date based on today
         * Example: Tomorrow, 2 days, 3 days, etc
         * @param date Date
         * @return Relative String
         */
        fun getRelativeDayString(date: Date?): String? {
            date?.let {
                return DateUtils.getRelativeTimeSpanString(
                    date.time,
                    System.currentTimeMillis(),
                    DateUtils.DAY_IN_MILLIS
                ).toString()
            }
            return null
        }

        /**
         * Checks if date is yesterday
         *
         * @param date Date
         * @return true if yesterday
         */
        fun isYesterday(date: Date): Boolean {
            return DateUtils.isToday(date.time + DateUtils.DAY_IN_MILLIS)
        }

        /**
         * Checks if date is today
         *
         * @param date Date
         * @return true if tomorrow
         */
        fun isTomorrow(date: Date): Boolean {
            return DateUtils.isToday(date.time - DateUtils.DAY_IN_MILLIS)
        }
    }
}