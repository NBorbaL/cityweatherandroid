package com.rbl.cityweatherandroid.utils

import java.text.DecimalFormat

class DoubleUtil {

    companion object {
        /**
         * Converts double to string value without trailing zeroes
         * Examples:
         * 20.0 -> 20
         * 12.9300 -> 12.93
         * 2.80 -> 2.8
         * @param double Double value
         * @return String without trailing zeroes
         */
        fun removeTrailingZeros(double: Double?): String? {
            double?.let {
                val decimalFormat = DecimalFormat("0.#")
                return decimalFormat.format(double)
            }
            return null
        }
    }
}