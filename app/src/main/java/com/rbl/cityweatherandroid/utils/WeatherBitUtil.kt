package com.rbl.cityweatherandroid.utils

import android.content.Context
import com.rbl.cityweatherandroid.data.model.WeatherBitLangEnum
import com.rbl.cityweatherandroid.data.model.WeatherBitUnitEnum
import java.util.*

class WeatherBitUtil {

    companion object {
        /**
         * Tries to find the drawable with the name of the icon supplied by the WeatherBit API
         *
         * @param context Context
         * @param iconName Icon name returned by the API
         * @return Icon Drawable Id
         */
        fun getWeatherIconResIdByIconName(context: Context, iconName: String?): Int? {
            iconName?.let {
                return context.resources.getIdentifier(
                    iconName, "drawable",
                    context.packageName
                )
            }
            return null
        }

        /**
         * Gets the device's locale and try to match it to a language that WeatherBit's API supports
         *
         * @return WeatherBit valid language parameter
         */
        fun getCorrespondingLanguageEnum(): WeatherBitLangEnum {
            when (Locale.getDefault()) {
                Locale.US -> {
                    return WeatherBitLangEnum.ENGLISH
                }
                Locale.ENGLISH -> {
                    return WeatherBitLangEnum.ENGLISH
                }
                Locale.CANADA -> {
                    return WeatherBitLangEnum.ENGLISH
                }
                Locale.CANADA_FRENCH -> {
                    return WeatherBitLangEnum.FRENCH
                }
                Locale.CHINA -> {
                    return WeatherBitLangEnum.CHINESE_SIMPLIFIED
                }
                Locale.CHINESE -> {
                    return WeatherBitLangEnum.CHINESE_SIMPLIFIED
                }
                Locale.FRANCE -> {
                    return WeatherBitLangEnum.FRENCH
                }
                Locale.FRENCH -> {
                    return WeatherBitLangEnum.FRENCH
                }
                Locale.GERMAN -> {
                    return WeatherBitLangEnum.GERMAN
                }
                Locale.GERMANY -> {
                    return WeatherBitLangEnum.GERMAN
                }
                Locale.ITALIAN -> {
                    return WeatherBitLangEnum.ITALIAN
                }
                Locale.ITALY -> {
                    return WeatherBitLangEnum.ITALIAN
                }
                Locale.JAPAN -> {
                    return WeatherBitLangEnum.ENGLISH
                }
                Locale.JAPANESE -> {
                    return WeatherBitLangEnum.ENGLISH
                }
                Locale.KOREA -> {
                    return WeatherBitLangEnum.ENGLISH
                }
                Locale.KOREAN -> {
                    return WeatherBitLangEnum.ENGLISH
                }
                Locale.PRC -> {
                    return WeatherBitLangEnum.CHINESE_SIMPLIFIED
                }
                Locale.SIMPLIFIED_CHINESE -> {
                    return WeatherBitLangEnum.CHINESE_SIMPLIFIED
                }
                Locale.TAIWAN -> {
                    return WeatherBitLangEnum.CHINESE_TRADITIONAL
                }
                Locale.TRADITIONAL_CHINESE -> {
                    return WeatherBitLangEnum.CHINESE_TRADITIONAL
                }
                Locale.UK -> {
                    return WeatherBitLangEnum.ENGLISH
                }
                else -> {
                    if (Locale.getDefault().language.toString()
                            .toLowerCase(Locale.getDefault()) == "pt"
                    ) {
                        return WeatherBitLangEnum.PORTUGUESE
                    }
                    return WeatherBitLangEnum.ENGLISH
                }
            }
        }

        /**
         * Gets the device locale and returns a valid unit to be used in WeatherBit's API
         *
         * @return Valid weatherbit unit parameter
         */
        fun getCorrespondingUnitEnum(): WeatherBitUnitEnum {
            return when (Locale.getDefault()) {
                Locale.US -> {
                    WeatherBitUnitEnum.FAHRENHEIT
                }
                else -> {
                    WeatherBitUnitEnum.METRIC
                }
            }
        }
    }
}