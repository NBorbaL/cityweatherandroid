package com.rbl.cityweatherandroid.data.model

import java.io.Serializable

data class Weather(
    val code: String?,
    val description: String?,
    val icon: String?,
    val drawableId: Int?
) : Serializable