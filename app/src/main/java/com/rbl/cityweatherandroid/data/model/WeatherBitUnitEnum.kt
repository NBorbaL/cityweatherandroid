package com.rbl.cityweatherandroid.data.model

enum class WeatherBitUnitEnum(val value: String) {
    METRIC("M"),
    SCIENTIFIC("S"),
    FAHRENHEIT("I")
}