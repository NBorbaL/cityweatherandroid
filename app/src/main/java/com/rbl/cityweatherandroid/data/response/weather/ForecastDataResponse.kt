package com.rbl.cityweatherandroid.data.response.weather

import android.content.Context
import com.rbl.cityweatherandroid.data.model.ForecastDetail
import com.rbl.cityweatherandroid.data.model.Weather
import com.rbl.cityweatherandroid.data.model.WeatherBitUnitEnum
import com.rbl.cityweatherandroid.utils.DateUtil
import com.rbl.cityweatherandroid.utils.WeatherBitUtil
import java.util.*

data class ForecastDataResponse(
    val app_max_temp: Double?,
    val app_min_temp: Double?,
    val clouds: Int?,
    val datetime: String?,
    val dewpt: Double?,
    val max_dhi: Double?,
    val max_temp: Double?,
    val min_temp: Double?,
    val moon_phase: Double?,
    val moonrise_ts: Long?,
    val moonset_ts: Long?,
    val pod: String?,
    val pop: Double?,
    val precip: Double?,
    val pres: Double?,
    val rh: Int?,
    val slp: Double?,
    val snow: Double?,
    val snow_depth: Double?,
    val sunrise_ts: Long?,
    val sunset_ts: Long?,
    val temp: Double?,
    val timestamp_local: String?,
    val timestamp_utc: String?,
    val ts: Double?,
    val uv: Double?,
    val vis: Double?,
    val weather: WeatherResponse?,
    val wind_cdir: String?,
    val wind_cdir_full: String?,
    val wind_dir: Int?,
    val wind_spd: Double?
) {
    fun getForecastDetail(
        context: Context,
        weatherBitUnitEnum: WeatherBitUnitEnum
    ): ForecastDetail {
        val date: Date? = DateUtil.convertStringToDate(datetime)
        val relativeDay: String? = DateUtil.getRelativeDayString(date)
        val formattedDate = DateUtil.dateToLocalFormatString(date)
        val dayOfWeek = DateUtil.getDayOfWeek(date, false)
        val dayOfWeekShort = DateUtil.getDayOfWeek(date, true)

        return ForecastDetail(
            weatherBitUnitEnum,
            relativeDay,
            date,
            formattedDate,
            dayOfWeek,
            dayOfWeekShort,
            temp,
            max_temp,
            min_temp,
            app_max_temp,
            app_min_temp,
            clouds,
            rh,
            pop,
            sunrise_ts.toString(),
            sunset_ts.toString(),
            moonrise_ts.toString(),
            moonset_ts.toString(),
            wind_spd,
            wind_cdir_full,
            Weather(
                weather?.code, weather?.description, weather?.icon,
                WeatherBitUtil.getWeatherIconResIdByIconName(context, weather?.icon)
            )
        )
    }
}