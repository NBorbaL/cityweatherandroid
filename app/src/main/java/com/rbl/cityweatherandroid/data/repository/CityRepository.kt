package com.rbl.cityweatherandroid.data.repository

import android.content.Context
import com.rbl.cityweatherandroid.data.model.City

interface CityRepository {
    fun getCities(context: Context?): List<City>?
}