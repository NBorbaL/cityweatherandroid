package com.rbl.cityweatherandroid.data.model

import java.io.Serializable

data class Forecast(
    val cityName: String?,
    val countryCode: String?,
    val forecastDetailList: ArrayList<ForecastDetail>?
) : Serializable