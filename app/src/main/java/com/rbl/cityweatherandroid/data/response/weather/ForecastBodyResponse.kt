package com.rbl.cityweatherandroid.data.response.weather

import android.content.Context
import com.rbl.cityweatherandroid.data.model.Forecast
import com.rbl.cityweatherandroid.data.model.ForecastDetail
import com.rbl.cityweatherandroid.data.model.WeatherBitUnitEnum

data class ForecastBodyResponse(
    val city_name: String?,
    val country_code: String?,
    val data: List<ForecastDataResponse>?,
    val lat: String?,
    val lon: String?,
    val state_code: String?,
    val timezone: String?
) {
    fun getForecastModel(context: Context, weatherBitUnitEnum: WeatherBitUnitEnum): Forecast {
        val dataList: ArrayList<ForecastDetail> = arrayListOf()
        if (data != null) {
            for (forecastData in data) {
                dataList.add(forecastData.getForecastDetail(context, weatherBitUnitEnum))
            }
        }
        return Forecast(city_name, country_code, dataList)
    }
}