package com.rbl.cityweatherandroid.data

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Retrofit singleton for WeatherBit API calls
 */
object WeatherApiService {

    private const val ENDPOINT = "https://api.weatherbit.io/v2.0/"

    private val moshi: Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    /**
     * Builds retrofit with the base url for WeatherBit
     * and adds moshi as its converter
     *
     * @return Retrofit instance
     */
    private fun initializeRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(ENDPOINT)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    /**
     * Retrofit weather api instance
     */
    val service: WeatherServices = initializeRetrofit().create(WeatherServices::class.java)
}

