package com.rbl.cityweatherandroid.data

import com.rbl.cityweatherandroid.data.model.Forecast

sealed class WeatherResult {
    class Success(val forecast: Forecast) : WeatherResult()
    class ApiError(val statusCode: Int?, val errorMessage: String) : WeatherResult()
    class Failure(val errorMessage: String?) : WeatherResult()
}