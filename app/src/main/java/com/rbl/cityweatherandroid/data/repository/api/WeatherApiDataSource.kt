package com.rbl.cityweatherandroid.data.repository.api

import android.content.Context
import com.rbl.cityweatherandroid.R
import com.rbl.cityweatherandroid.data.WeatherApiService
import com.rbl.cityweatherandroid.data.WeatherResult
import com.rbl.cityweatherandroid.data.repository.WeatherRepository
import com.rbl.cityweatherandroid.data.response.weather.ForecastBodyResponse
import com.rbl.cityweatherandroid.utils.WeatherBitUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WeatherApiDataSource :
    WeatherRepository {

    /**
     * Gets the city forecast via WeatherBit's API
     *
     * @param context Context
     * @param cityName City Name
     * @param countryCode Country Code - Example: BR
     * @param days How many days of forecast
     * @param resultCallback Callback for operation result
     */
    override fun getCityForecast(
        context: Context?,
        cityName: String,
        countryCode: String,
        days: Int,
        resultCallback: (result: WeatherResult) -> Unit
    ) {
        if (context != null) {
            val weatherBitUnitEnum = WeatherBitUtil.getCorrespondingUnitEnum()

            WeatherApiService.service.getDailyCityForecast(
                context.getString(R.string.weather_bit_api_key),
                WeatherBitUtil.getCorrespondingLanguageEnum().value,
                weatherBitUnitEnum.value,
                days,
                cityName,
                countryCode
            ).enqueue(object : Callback<ForecastBodyResponse> {
                override fun onFailure(call: Call<ForecastBodyResponse>, t: Throwable) {
                    resultCallback(WeatherResult.Failure(t.message))
                }

                override fun onResponse(
                    call: Call<ForecastBodyResponse>,
                    response: Response<ForecastBodyResponse>
                ) {
                    if (response.isSuccessful) {
                        response.body()?.let { forecastBodyResponse ->
                            resultCallback(
                                WeatherResult.Success(
                                    forecastBodyResponse.getForecastModel(
                                        context,
                                        weatherBitUnitEnum
                                    )
                                )
                            )
                        }
                    } else {
                        resultCallback(
                            WeatherResult.ApiError(
                                response.code(),
                                response.message()
                            )
                        )
                    }
                }
            })
        } else {
            resultCallback(WeatherResult.Failure("No context received"))
        }
    }
}