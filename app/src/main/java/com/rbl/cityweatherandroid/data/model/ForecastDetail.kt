package com.rbl.cityweatherandroid.data.model

import java.io.Serializable
import java.util.*

data class ForecastDetail(
    val weatherBitUnitEnum: WeatherBitUnitEnum?,
    val relativeDay: String?,
    val dateTime: Date?,
    val formattedDateTime: String?,
    val dayOfWeek: String?,
    val dayOfWeekShort: String?,
    val averageTemp: Double?,
    val maxTemp: Double?,
    val minTemp: Double?,
    val appMaxTemp: Double?,
    val appMinTemp: Double?,
    val cloudCoverPercentage: Int?,
    val relativeHumidity: Int?,
    val precipitationChancePercent: Double?,
    val sunriseDateTime: String?,
    val sunsetDateTime: String?,
    val moonriseDateTime: String?,
    val moonsetDateTime: String?,
    val windSpeed: Double?,
    val windDirectionDescription: String?,
    val weather: Weather?,
    var isSelected: Boolean = false
) : Serializable