package com.rbl.cityweatherandroid.data.response.weather

data class WeatherResponse(
    val code: String?,
    val description: String?,
    val icon: String?
)