package com.rbl.cityweatherandroid.data.model

data class City(
    val city_id: Int?,
    val city_name: String?,
    val country_code: String?,
    val country_full: String?,
    val lat: Double?,
    val lon: Double?,
    val state_code: String?
) {
    override fun toString(): String {
        return "$city_name ($country_full)"
    }
}