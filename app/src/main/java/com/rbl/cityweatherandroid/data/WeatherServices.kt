package com.rbl.cityweatherandroid.data

import com.rbl.cityweatherandroid.data.model.WeatherBitLangEnum
import com.rbl.cityweatherandroid.data.model.WeatherBitUnitEnum
import com.rbl.cityweatherandroid.data.response.weather.ForecastBodyResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherServices {
    @GET("forecast/daily")
    fun getDailyCityForecast(
        @Query("key") key: String,
        @Query("lang") lang: String = WeatherBitLangEnum.ENGLISH.value,
        @Query("units") units: String = WeatherBitUnitEnum.METRIC.value,
        @Query("days") days: Int,
        @Query("city") cityName: String,
        @Query("country") countryCode: String
    ): Call<ForecastBodyResponse>
}