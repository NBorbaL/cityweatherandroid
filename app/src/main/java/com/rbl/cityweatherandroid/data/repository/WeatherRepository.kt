package com.rbl.cityweatherandroid.data.repository

import android.content.Context
import com.rbl.cityweatherandroid.data.WeatherResult

interface WeatherRepository {
    /**
     * Gets the forecast for a specific city
     *
     * @param context Context
     * @param cityName City name
     * @param countryCode Country code
     * @param days Number of days of forecast (Max 16)
     * @param resultCallback Callback for result
     */
    fun getCityForecast(
        context: Context?,
        cityName: String,
        countryCode: String,
        days: Int,
        resultCallback: (result: WeatherResult) -> Unit
    )
}