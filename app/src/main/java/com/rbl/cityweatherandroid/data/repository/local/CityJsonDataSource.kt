package com.rbl.cityweatherandroid.data.repository.local

import android.content.Context
import com.rbl.cityweatherandroid.R
import com.rbl.cityweatherandroid.data.model.City
import com.rbl.cityweatherandroid.data.repository.CityRepository
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

class CityJsonDataSource :
    CityRepository {
    /**
     * Get all cities from local JSON supplied by WeatherBit
     *
     * @param context Context
     * @return List of cities
     */
    override fun getCities(context: Context?): List<City>? {
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
        val listType = Types.newParameterizedType(List::class.java, City::class.java)
        val adapter: JsonAdapter<List<City>> = moshi.adapter(listType)
        context?.let {
            return adapter.fromJson(context.resources.openRawResource(R.raw.cities_20000)
                .bufferedReader()
                .use { it.readText() })
        }
        return null
    }
}