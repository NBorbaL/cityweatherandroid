package com.rbl.cityweatherandroid.presentation.weather

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.lifecycle.Observer
import com.rbl.cityweatherandroid.R
import com.rbl.cityweatherandroid.data.model.City
import com.rbl.cityweatherandroid.data.repository.api.WeatherApiDataSource
import com.rbl.cityweatherandroid.data.repository.local.CityJsonDataSource
import com.rbl.cityweatherandroid.extensions.hideKeyboard
import com.rbl.cityweatherandroid.presentation.IgnoreAccentsArrayAdapter
import com.rbl.cityweatherandroid.presentation.base.BaseActivity
import com.rbl.cityweatherandroid.presentation.weather_detail.WeatherDetailActivity
import kotlinx.android.synthetic.main.activity_weather_search.*
import kotlinx.android.synthetic.main.include_toolbar.*

class WeatherSearchActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather_search)
        setupToolbar(toolbar, R.string.weather_search_title)
        setupTitleStyle()
        // Initializes this activity's view model
        val viewModel: WeatherSearchViewModel =
            WeatherSearchViewModel.ViewModelFactory(
                application,
                WeatherApiDataSource(),
                CityJsonDataSource()
            ).create(WeatherSearchViewModel::class.java)
        setupAutoCompleteTextViewClick(viewModel)
        // Sets up the view model observers for ui updates
        setupViewModelObservers(viewModel)
        setupButtonClickListeners(viewModel)
        // Gets all cities
        viewModel.getCities()
    }

    /**
     * Sets up title style with html since it has two colors on the same text
     *
     */
    private fun setupTitleStyle() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textViewQuestionHtml.text = Html.fromHtml(
                getString(R.string.weather_search_question),
                Html.FROM_HTML_MODE_LEGACY
            )
        } else {
            textViewQuestionHtml.text = Html.fromHtml(getString(R.string.weather_search_question))
        }
    }

    /**
     * Calls the view model getCityForecast function and hides keyboard
     *
     * @param viewModel viewModel
     */
    private fun getCityForecast(city: City, viewModel: WeatherSearchViewModel) {
        viewModel.getCityForecast(city, false)
        hideKeyboard()
    }

    /**
     * Calls the view model getCityForecast function and hides keyboard
     *
     * @param viewModel viewModel
     */
    private fun retryGetCityForecast(viewModel: WeatherSearchViewModel) {
        viewModel.getCityForecast(false)
        hideKeyboard()
    }

    /**
     * Sets up click listener for the city's AutoCompleteTextView
     *
     * @param viewModel viewModel
     */
    private fun setupAutoCompleteTextViewClick(viewModel: WeatherSearchViewModel) {
        textViewCity.setOnItemClickListener { parent, view, position, id ->
            val selectedCity: City = parent.adapter.getItem(position) as City
            getCityForecast(selectedCity, viewModel)
        }
    }

    /**
     * Sets up all observers actions
     *
     * @param viewModel viewModel
     */
    private fun setupViewModelObservers(viewModel: WeatherSearchViewModel) {
        // Observes when the viewFlipper should change its current view
        viewModel.viewFlipperLiveData.observe(this, Observer { pair ->
            // 0 means that we want to hide the view flipper
            if (pair.first == WeatherSearchViewModel.VIEW_FLIPPER_INVISIBLE) {
                linearLayoutViews.visibility = View.GONE
            } else {
                linearLayoutViews.visibility = View.VISIBLE
                if (pair.first == WeatherSearchViewModel.VIEW_FLIPPER_LOADING) {
                    startLoadingAnimation()
                    includeLoadingAnimation.visibility = View.VISIBLE
                    linearLayoutError.visibility = View.GONE
                } else if (pair.first == WeatherSearchViewModel.VIEW_FLIPPER_ERROR) {
                    stopLoadingAnimation()
                    linearLayoutError.visibility = View.VISIBLE
                    includeLoadingAnimation.visibility = View.GONE
                }
            }
        })

        // Observes when the weather data changes
        viewModel.weatherLiveData.observe(this, Observer { forecast ->
            forecast?.let {
                startActivity(WeatherDetailActivity.getStartIntent(this, forecast))
            }
        })

        // Observes when the city data changes
        viewModel.cityLiveData.observe(this, Observer { cityList ->
            cityList?.let {
                val adapter: IgnoreAccentsArrayAdapter<City> = IgnoreAccentsArrayAdapter(
                    this,
                    android.R.layout.simple_dropdown_item_1line,
                    cityList
                )
                textViewCity.setAdapter(adapter)
            }
        })
    }

    /**
     * Sets up all button click listeners
     *
     * @param viewModel viewModel
     */
    private fun setupButtonClickListeners(viewModel: WeatherSearchViewModel) {
        buttonRetry.setOnClickListener {
            retryGetCityForecast(viewModel)
        }
    }
}
