package com.rbl.cityweatherandroid.presentation.base

import android.graphics.drawable.AnimationDrawable
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.rbl.cityweatherandroid.R
import kotlinx.android.synthetic.main.include_loading_animation.*

/**
 * Activities should implement this base activity for shared functionality
 */
open class BaseActivity : AppCompatActivity() {

    private lateinit var animationDrawable: AnimationDrawable

    /**
     * Sets up toolbar for usage in activity
     *
     * @param toolbar Toolbar to be used as an action bar replacement
     * @param titleResId String resource id for toolbar title
     * @param shouldShowBackButton True = Shows back button <br/> False = Does not show back button
     */
    fun setupToolbar(toolbar: Toolbar, titleResId: Int, shouldShowBackButton: Boolean = false) {
        toolbar.title = getString(titleResId)
        setSupportActionBar(toolbar)
        if (shouldShowBackButton) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_button_primary)
        }
    }

    /**
     * Sets up toolbar for usage in activity
     *
     * @param toolbar Toolbar to be used as an action bar replacement
     * @param titleString Title string
     * @param shouldShowBackButton True = Shows back button <br/> False = Does not show back button
     */
    fun setupToolbar(toolbar: Toolbar, titleString: String, shouldShowBackButton: Boolean = false) {
        toolbar.title = titleString
        setSupportActionBar(toolbar)
        if (shouldShowBackButton) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_button_primary)
        }
    }

    /**
     * Rewrite onSupportNavigateUp default functionality for back button
     * on toolbar to go back to previous activity
     * @return Has been handled
     */
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setupLoadingAnimation() {
        if (imageViewLoading != null) {
            animationDrawable = imageViewLoading.background as AnimationDrawable
        }
    }

    fun startLoadingAnimation() {
        if (!this::animationDrawable.isInitialized) {
            setupLoadingAnimation()
        }

        if (this::animationDrawable.isInitialized) {
            animationDrawable.start()
        }
    }

    fun stopLoadingAnimation() {
        if (!this::animationDrawable.isInitialized) {
            setupLoadingAnimation()
        }

        if (this::animationDrawable.isInitialized) {
            animationDrawable.stop()
        }
    }
}