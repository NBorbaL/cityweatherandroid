package com.rbl.cityweatherandroid.presentation.weather_detail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.rbl.cityweatherandroid.R
import com.rbl.cityweatherandroid.data.model.ForecastDetail
import com.rbl.cityweatherandroid.data.model.WeatherBitUnitEnum
import com.rbl.cityweatherandroid.utils.DoubleUtil
import kotlinx.android.synthetic.main.item_forecast_detail.view.*
import java.util.*


class ForecastDetailAdapter(
    private val context: Context,
    private val forecastDetailList: ArrayList<ForecastDetail>,
    private val onItemClickListener: ((forecastDetail: ForecastDetail) -> Unit)
) : RecyclerView.Adapter<ForecastDetailAdapter.ForecastDetailViewHolder>() {

    private var selectedForecastDetail: ForecastDetail? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastDetailViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_forecast_detail, parent, false)
        return ForecastDetailViewHolder(context, view, onItemClickListener)
    }

    override fun getItemCount() = forecastDetailList.count()

    override fun onBindViewHolder(holder: ForecastDetailViewHolder, position: Int) {
        holder.bindView(forecastDetailList[position])
    }

    /**
     * Changes the current selected forecast.isSelected to false,
     * selects the new one, and notifies the view
     *
     * @param forecastDetail forecast that should be selected
     */
    fun selectForecast(forecastDetail: ForecastDetail) {
        selectedForecastDetail?.isSelected = false
        forecastDetail.isSelected = true
        for (i in 0 until forecastDetailList.size) {
            if (selectedForecastDetail != null) {
                if (selectedForecastDetail!! == forecastDetailList[i]) {
                    notifyItemChanged(i)
                }
            }

            if (forecastDetail == forecastDetailList[i]) {
                notifyItemChanged(i)
            }
        }
        selectedForecastDetail = forecastDetail
    }

    class ForecastDetailViewHolder(
        private val context: Context,
        itemView: View,
        private val onItemClickListener: (forecastDetail: ForecastDetail) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {
        private val dayOfWeek = itemView.textViewDayOfWeek
        private val chanceOfRain = itemView.textViewChanceOfRain
        private val weatherDescription = itemView.textViewWeatherDescription
        private val maxTemp = itemView.textViewMaxTemp
        private val minTemp = itemView.textViewMinTemp
        private val viewSelectedIndicator = itemView.viewSelectedIndicator

        fun bindView(forecastDetail: ForecastDetail) {
            dayOfWeek.text = context.getString(
                R.string.weather_detail_day_of_week_and_date,
                forecastDetail.dayOfWeekShort?.toUpperCase(Locale.getDefault()),
                forecastDetail.relativeDay
            )
            chanceOfRain.text = context.getString(
                R.string.common_percent_placeholder,
                DoubleUtil.removeTrailingZeros(forecastDetail.precipitationChancePercent)
            )
            weatherDescription.text = forecastDetail.weather?.description
            forecastDetail.weather?.drawableId?.let { drawableId ->
                weatherDescription.setCompoundDrawablesWithIntrinsicBounds(drawableId, 0, 0, 0)
            }

            // Gets the correct temperature placeholder
            var temperaturePlaceholderResId: Int = R.string.common_temperature_celsius_placeholder
            when (forecastDetail.weatherBitUnitEnum) {
                WeatherBitUnitEnum.METRIC -> {
                    temperaturePlaceholderResId = R.string.common_temperature_celsius_placeholder
                }
                WeatherBitUnitEnum.FAHRENHEIT -> {
                    temperaturePlaceholderResId = R.string.common_temperature_fahrenheit_placeholder
                }
                WeatherBitUnitEnum.SCIENTIFIC -> {
                    temperaturePlaceholderResId = R.string.common_temperature_kelvin_placeholder
                }
            }
            maxTemp.text = context.getString(
                temperaturePlaceholderResId,
                DoubleUtil.removeTrailingZeros(forecastDetail.maxTemp)
            )
            minTemp.text = context.getString(
                temperaturePlaceholderResId,
                DoubleUtil.removeTrailingZeros(forecastDetail.minTemp)
            )
            if (forecastDetail.isSelected) {
                slideIn(viewSelectedIndicator)
            } else {
                slideOut(viewSelectedIndicator)
            }
            itemView.setOnClickListener {
                onItemClickListener(forecastDetail)
            }
        }

        /**
         * Animates the supplied view with a left to right animation
         *
         * @param currentView View to animate
         */
        private fun slideIn(
            currentView: View
        ) {
            // Only animates entrance if view is not shown yet
            if (currentView.visibility == View.GONE) {
                currentView.visibility = View.VISIBLE
                val animation: Animation = AnimationUtils.loadAnimation(
                    context,
                    R.anim.left_to_right
                )
                currentView.startAnimation(animation)
            }
        }

        /**
         * Animates the supplied view with a right to left animation
         *
         * @param currentView View to animate
         */
        private fun slideOut(
            currentView: View
        ) {
            // Only animates entrance if view is not shown yet
            if (currentView.visibility == View.VISIBLE) {
                val animation: Animation = AnimationUtils.loadAnimation(
                    context,
                    R.anim.right_to_left
                )
                animation.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationRepeat(p0: Animation?) {
                    }

                    override fun onAnimationEnd(p0: Animation?) {
                        currentView.visibility = View.GONE
                    }

                    override fun onAnimationStart(p0: Animation?) {
                    }

                })
                currentView.startAnimation(animation)
            }
        }
    }
}