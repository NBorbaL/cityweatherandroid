package com.rbl.cityweatherandroid.presentation.weather

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rbl.cityweatherandroid.data.WeatherResult
import com.rbl.cityweatherandroid.data.model.City
import com.rbl.cityweatherandroid.data.model.Forecast
import com.rbl.cityweatherandroid.data.repository.CityRepository
import com.rbl.cityweatherandroid.data.repository.WeatherRepository
import com.rbl.cityweatherandroid.utils.DateUtil
import java.lang.ref.WeakReference

class WeatherSearchViewModel(
    application: Application,
    private val weatherDataSource: WeatherRepository,
    private val cityDataSource: CityRepository
) : AndroidViewModel(application) {

    companion object {
        const val VIEW_FLIPPER_INVISIBLE = 0
        const val VIEW_FLIPPER_LOADING = 1
        const val VIEW_FLIPPER_ERROR = 2
    }

    private val weakContext: WeakReference<Context> =
        WeakReference(getApplication<Application>().applicationContext)
    val weatherLiveData: MutableLiveData<Forecast> = MutableLiveData()
    val viewFlipperLiveData: MutableLiveData<Pair<Int, String?>> = MutableLiveData()
    val cityLiveData: MutableLiveData<List<City>?> = MutableLiveData()

    private var selectedCity: City? = null

    /**
     * Retrieves all the cities found via the provided data source
     *
     */
    fun getCities() {
        // Make the call and suspend execution until it finishes
        val result = cityDataSource.getCities(weakContext.get())
        // Return the result of the request
        cityLiveData.value = result
    }

    private fun setCity(city: City) {
        selectedCity = city
    }

    fun getSelectedCity() = selectedCity

    fun getCityForecast(onlyTomorrow: Boolean) {
        selectedCity?.let { city ->
            getCityForecast(city, onlyTomorrow)
        }
    }

    /**
     * Retrieves the city forecast via the provided data source
     *
     * @param onlyTomorrow true : return only tomorrow's forecast
     */
    fun getCityForecast(city: City, onlyTomorrow: Boolean) {
        viewFlipperLiveData.value = Pair(VIEW_FLIPPER_LOADING, null)
        setCity(city)
        selectedCity?.let { selectedCity ->
            if (!selectedCity.city_name.isNullOrEmpty() && !selectedCity.country_code.isNullOrEmpty()) {
                weatherDataSource.getCityForecast(
                    weakContext.get(), selectedCity.city_name, selectedCity.country_code, 7
                ) { result: WeatherResult ->
                    when (result) {
                        is WeatherResult.Success -> {
                            if (onlyTomorrow) {
                                // Removes all forecast data that is not from today
                                result.forecast.forecastDetailList?.let { forecastDetailList ->
                                    for (i in forecastDetailList.size - 1 downTo 0) {
                                        if (forecastDetailList[i].formattedDateTime.isNullOrEmpty()
                                            || forecastDetailList[i].dateTime == null
                                        ) {
                                            forecastDetailList.removeAt(i)
                                        }

                                        // If dateTime is not null
                                        // we should check if its date is tomorrows
                                        forecastDetailList[i].dateTime?.let { date ->
                                            if (!DateUtil.isTomorrow(date)) {
                                                forecastDetailList.removeAt(i)
                                            }
                                        }
                                    }
                                }
                            }
                            // Hides loading when going to next activity
                            viewFlipperLiveData.value = Pair(VIEW_FLIPPER_INVISIBLE, null)
                            weatherLiveData.value = result.forecast
                        }
                        is WeatherResult.ApiError -> {
                            // TODO: SHOULD LOG ERROR
                            viewFlipperLiveData.value =
                                Pair(VIEW_FLIPPER_ERROR, result.errorMessage)
                        }
                        is WeatherResult.Failure -> {
                            // TODO: SHOULD LOG ERROR
                            viewFlipperLiveData.value =
                                Pair(VIEW_FLIPPER_ERROR, result.errorMessage)
                        }
                    }
                }
            }
        }
    }

    class ViewModelFactory(
        private val application: Application,
        private val weatherDataSource: WeatherRepository,
        private val cityDataSource: CityRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(WeatherSearchViewModel::class.java)) {
                return WeatherSearchViewModel(application, weatherDataSource, cityDataSource) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}