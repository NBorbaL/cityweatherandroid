package com.rbl.cityweatherandroid.presentation.weather_detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rbl.cityweatherandroid.R
import com.rbl.cityweatherandroid.data.model.Forecast
import com.rbl.cityweatherandroid.data.model.ForecastDetail
import com.rbl.cityweatherandroid.data.model.WeatherBitUnitEnum
import com.rbl.cityweatherandroid.presentation.base.BaseActivity
import com.rbl.cityweatherandroid.utils.DateUtil
import com.rbl.cityweatherandroid.utils.DoubleUtil
import kotlinx.android.synthetic.main.activity_weather_detail.*
import kotlinx.android.synthetic.main.include_toolbar.*

class WeatherDetailActivity : BaseActivity() {

    private var forecastDetailAdapter: ForecastDetailAdapter? = null

    companion object {
        private const val EXTRA_FORECAST_OBJECT = "FORECAST_OBJECT"

        fun getStartIntent(context: Context, forecast: Forecast): Intent {
            return Intent(context, WeatherDetailActivity::class.java).apply {
                putExtra(EXTRA_FORECAST_OBJECT, forecast)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather_detail)
        setupToolbar(toolbar, R.string.weather_detail_title, true)
        validateIntentData()?.let { forecast ->
            showForecastData(forecast)
        }
    }

    /**
     * Checks if the supplied intent is valid and has a forecast object
     * If it doesn't, activity is finished
     * @return Supplied forecast via Intent
     */
    private fun validateIntentData(): Forecast? {
        if (intent.hasExtra(EXTRA_FORECAST_OBJECT)) {
            return intent.getSerializableExtra(EXTRA_FORECAST_OBJECT) as Forecast
        } else {
            finish()
        }
        return null
    }

    /**
     * Updates the main views to show the forecast data
     * and adapts the recyclerview with the forecast details
     *
     * @param forecast Forecast object
     */
    private fun showForecastData(forecast: Forecast) {
        showMainForecastData(forecast)
        forecast.forecastDetailList?.let { forecastDetailList ->
            with(recyclerViewForecastData) {
                layoutManager =
                    LinearLayoutManager(this@WeatherDetailActivity, RecyclerView.VERTICAL, false)
                setHasFixedSize(true)
                adapter = ForecastDetailAdapter(
                    this@WeatherDetailActivity,
                    forecastDetailList
                ) { forecastDetail ->
                    showForecastDetail(forecastDetail)
                }
                forecastDetailAdapter = adapter as ForecastDetailAdapter
            }
            // Shows first day available on forecast
            if (forecastDetailList.size > 0) {
                showForecastDetail(getDefaultForecastDetail(forecastDetailList))
            }
        }
    }

    /**
     * Returns the default forecast object to show when the activity is first opened
     * Will try to return tomorrow's forecast as default
     * @return Default forecast that should be shown
     */
    private fun getDefaultForecastDetail(forecastDetailList: ArrayList<ForecastDetail>?): ForecastDetail? {
        forecastDetailList?.let {
            if (forecastDetailList.size > 0) {
                for (i in 0 until forecastDetailList.size) {
                    forecastDetailList[i].dateTime?.let { date ->
                        if (DateUtil.isTomorrow(date)) {
                            return forecastDetailList[i]
                        }
                    }
                }
                // Returns first item in case none are tomorrow
                return forecastDetailList[0]
            }
            return null
        }
        return null
    }

    /**
     * Updates the views with the main forecast data
     *
     * @param forecast
     */
    private fun showMainForecastData(forecast: Forecast) {
        // Sets toolbar title to the current city
        setupToolbar(
            toolbar, getString(
                R.string.weather_detail_city_country, forecast.cityName, forecast.countryCode
            ), true
        )
    }

    /**
     * Updates the views that use the forecast details
     *
     * @param forecastDetail Forecast detail model
     */
    private fun showForecastDetail(forecastDetail: ForecastDetail?) {
        forecastDetail?.let {
            textViewRelativeDay.text = forecastDetail.relativeDay
            textViewDaySubtitle.text = getString(
                R.string.weather_detail_subtitle,
                forecastDetail.dayOfWeek,
                forecastDetail.formattedDateTime
            )

            textViewTemperatureMin.text = getString(
                R.string.weather_detail_temp_placeholder,
                DoubleUtil.removeTrailingZeros(forecastDetail.minTemp)
            )
            textViewTemperatureMax.text = getString(
                R.string.weather_detail_temp_placeholder,
                DoubleUtil.removeTrailingZeros(forecastDetail.maxTemp)
            )

            textViewRainChancePercent.text = getString(
                R.string.weather_detail_rain_chance,
                DoubleUtil.removeTrailingZeros(forecastDetail.precipitationChancePercent)
            )
            textViewCloudCoverPercent.text = getString(
                R.string.weather_detail_cloud_cover,
                forecastDetail.cloudCoverPercentage.toString()
            )

            var windStringResId: Int = R.string.weather_detail_wind_direction_and_speed_metric
            var temperatureSymbolResId: Int = R.string.common_temperature_celsius_symbol

            // Select correct symbol for the current Unit
            when (forecastDetail.weatherBitUnitEnum) {
                WeatherBitUnitEnum.METRIC -> {
                    windStringResId = R.string.weather_detail_wind_direction_and_speed_metric
                    temperatureSymbolResId = R.string.common_temperature_celsius_symbol
                }
                WeatherBitUnitEnum.FAHRENHEIT -> {
                    windStringResId = R.string.weather_detail_wind_direction_and_speed_fahrenheit
                    temperatureSymbolResId = R.string.common_temperature_fahrenheit_symbol
                }
                WeatherBitUnitEnum.SCIENTIFIC -> {
                    windStringResId = R.string.weather_detail_wind_direction_and_speed_scientific
                    temperatureSymbolResId = R.string.common_temperature_kelvin_symbol
                }
            }

            // Use correct string for weather speed unit
            textViewWindDirectionAndSpeed.text = getString(
                windStringResId,
                forecastDetail.windDirectionDescription.toString(),
                DoubleUtil.removeTrailingZeros(forecastDetail.windSpeed)
            )

            // Use correct symbol for the current Unit
            textViewTemperatureMinSymbol.text = getString(temperatureSymbolResId)
            textViewTemperatureMaxSymbol.text = getString(temperatureSymbolResId)

            forecastDetail.weather?.let { weather ->
                textViewWeatherDescription.text = weather.description
                weather.drawableId?.let { weatherIconId ->
                    imageViewWeatherIcon.setImageDrawable(
                        ResourcesCompat.getDrawable(
                            resources,
                            weatherIconId,
                            null
                        )
                    )
                }
            }
            forecastDetailAdapter?.selectForecast(forecastDetail)
        }
    }
}