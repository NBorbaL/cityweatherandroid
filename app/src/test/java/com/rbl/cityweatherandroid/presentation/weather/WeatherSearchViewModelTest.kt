package com.rbl.cityweatherandroid.presentation.weather

import android.app.Application
import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.verify
import com.rbl.cityweatherandroid.data.WeatherResult
import com.rbl.cityweatherandroid.data.model.City
import com.rbl.cityweatherandroid.data.model.Forecast
import com.rbl.cityweatherandroid.data.repository.CityRepository
import com.rbl.cityweatherandroid.data.repository.WeatherRepository
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class WeatherSearchViewModelTest {

    // Executes test on main thread
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var weatherLiveDataObserver: Observer<Forecast>

    @Mock
    private lateinit var cityLiveDataObserver: Observer<List<City>?>

    @Mock
    private lateinit var viewFlipperDataObserver: Observer<Pair<Int, String?>>

    private lateinit var viewModel: WeatherSearchViewModel
    private var application: Application = Mockito.mock(Application::class.java)

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `viewModel getCities is successful then set cityLiveData`() {
        // Arrange
        val cityList = listOf(
            City(1, "City", "BR", "Brazil", 0.0, 0.0, "")
        )
        viewModel = WeatherSearchViewModel(
            application, MockWeatherRepository(
                WeatherResult.Success(
                    Forecast(null, null, null)
                )
            ), MockCityRepository(cityList)
        )
        viewModel.cityLiveData.observeForever(cityLiveDataObserver)

        // Act
        viewModel.getCities()

        // Assert
        verify(cityLiveDataObserver).onChanged(cityList)
    }

    @Test
    fun `viewModel getCityForecast is successful then set correct weatherLiveData`() {
        // Arrange
        val city = City(1, "City", "BR", "Brazil", 0.0, 0.0, "")
        val forecast = Forecast("Curitiba", "BR", null)
        viewModel = WeatherSearchViewModel(
            application, MockWeatherRepository(WeatherResult.Success(forecast)),
            MockCityRepository(
                listOf(
                    city
                )
            )
        )
        viewModel.weatherLiveData.observeForever(weatherLiveDataObserver)
        viewModel.viewFlipperLiveData.observeForever(viewFlipperDataObserver)

        // Act
        viewModel.getCityForecast(city, false)

        verify(weatherLiveDataObserver).onChanged(forecast)
        verify(viewFlipperDataObserver).onChanged(
            Pair(
                WeatherSearchViewModel.VIEW_FLIPPER_INVISIBLE,
                null
            )
        )
    }

    @Test
    fun `viewModel getCityForecast has api error then set correct weatherLiveData`() {
        // Arrange
        val errorMessage = "API_ERROR"
        val city = City(1, "City", "BR", "Brazil", 0.0, 0.0, "")
        viewModel = WeatherSearchViewModel(
            application, MockWeatherRepository(WeatherResult.ApiError(400, errorMessage)),
            MockCityRepository(
                listOf(
                    city
                )
            )
        )
        viewModel.viewFlipperLiveData.observeForever(viewFlipperDataObserver)

        // Act
        viewModel.getCityForecast(city, false)

        verify(viewFlipperDataObserver).onChanged(
            Pair(
                WeatherSearchViewModel.VIEW_FLIPPER_ERROR,
                errorMessage
            )
        )
    }

    @Test
    fun `viewModel getCityForecast has failure then set correct weatherLiveData`() {
        // Arrange
        val errorMessage = "FAILURE"
        val city = City(1, "City", "BR", "Brazil", 0.0, 0.0, "")
        viewModel = WeatherSearchViewModel(
            application, MockWeatherRepository(WeatherResult.Failure(errorMessage)),
            MockCityRepository(
                listOf(
                    city
                )
            )
        )
        viewModel.viewFlipperLiveData.observeForever(viewFlipperDataObserver)

        // Act
        viewModel.getCityForecast(city, false)

        verify(viewFlipperDataObserver).onChanged(
            Pair(
                WeatherSearchViewModel.VIEW_FLIPPER_ERROR,
                errorMessage
            )
        )
    }

    class MockWeatherRepository(private val result: WeatherResult) : WeatherRepository {
        override fun getCityForecast(
            context: Context?,
            cityName: String,
            countryCode: String,
            days: Int,
            resultCallback: (result: WeatherResult) -> Unit
        ) {
            resultCallback(result)
        }
    }


    class MockCityRepository(private val cityList: List<City>) : CityRepository {
        override fun getCities(context: Context?): List<City>? {
            return cityList
        }
    }
}