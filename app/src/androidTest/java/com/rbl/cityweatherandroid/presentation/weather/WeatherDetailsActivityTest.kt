package com.rbl.cityweatherandroid.presentation.weather


import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.rbl.cityweatherandroid.R
import com.rbl.cityweatherandroid.data.model.Forecast
import com.rbl.cityweatherandroid.data.model.ForecastDetail
import com.rbl.cityweatherandroid.data.model.Weather
import com.rbl.cityweatherandroid.data.model.WeatherBitUnitEnum
import com.rbl.cityweatherandroid.presentation.weather_detail.WeatherDetailActivity
import com.rbl.cityweatherandroid.utils.DoubleUtil
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.instanceOf
import org.hamcrest.TypeSafeMatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
class WeatherDetailsActivityTest {

    private val defaultForecast = Forecast(
        "Curitiba", "BR", arrayListOf(
            ForecastDetail(
                WeatherBitUnitEnum.METRIC, "Today",
                null, "01/01/1900", "Monday", "Mon", 0.1,
                0.2, 0.3, 0.4, 0.5, 1, 2, 0.6,
                null, null, null, null, 0.7, "East",
                Weather("a01d", "Sunny", "a01d", R.drawable.a01d), false
            ),
            ForecastDetail(
                WeatherBitUnitEnum.FAHRENHEIT, "Tomorrow",
                null, "02/01/1900", "Tuesday", "TUE", 0.8,
                0.9, 1.1, 1.2, 1.3, 3, 4, 1.4,
                null, null, null, null, 1.5, "West",
                Weather("a01d", "Overcast", "a01d", R.drawable.a01d), false
            )
        )
    )

    @Rule
    @JvmField
    var mActivityRule: ActivityTestRule<WeatherDetailActivity> =
        object : ActivityTestRule<WeatherDetailActivity>(
            WeatherDetailActivity::class.java
        ) {
            override fun getActivityIntent(): Intent {
                // Will open the activity with the intent it is expecting
                targetContext =
                    InstrumentationRegistry.getInstrumentation()
                        .targetContext
                return WeatherDetailActivity.getStartIntent(targetContext, defaultForecast)
            }
        }

    private lateinit var instrumentationContext: Context
    lateinit var targetContext: Context

    @Before
    fun setup() {
        instrumentationContext = InstrumentationRegistry.getInstrumentation().context
        Intents.init();
    }

    @After
    fun tearDown() {
        Intents.release();
    }

    @Test
    fun checkIfDetailActivityIsShowingForecastCorrectly() {
        // Checks if toolbar data is correct
        onView(
            allOf(
                instanceOf(TextView::class.java),
                withParent(withId(R.id.toolbar))
            )
        )
            .check(
                matches(
                    withText(
                        targetContext.getString(
                            R.string.weather_detail_city_country,
                            defaultForecast.cityName,
                            defaultForecast.countryCode
                        )
                    )
                )
            )

        // Checks if current data on screen is correct
        matchViewableWeatherWithObject(defaultForecast.forecastDetailList?.get(0))
        // Clicks on the second item of the recycler view
        // And checks if it is showing properly as well
        onView(withId(R.id.recyclerViewForecastData))
            .perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()));
        matchViewableWeatherWithObject(defaultForecast.forecastDetailList?.get(1))
    }

    private fun matchViewableWeatherWithObject(item: ForecastDetail?) {
        var windStringResId: Int = R.string.weather_detail_wind_direction_and_speed_metric
        var temperatureSymbolResId: Int = R.string.common_temperature_celsius_symbol

        // Select correct symbol for the current Unit
        when (item?.weatherBitUnitEnum) {
            WeatherBitUnitEnum.METRIC -> {
                windStringResId = R.string.weather_detail_wind_direction_and_speed_metric
                temperatureSymbolResId = R.string.common_temperature_celsius_symbol
            }
            WeatherBitUnitEnum.FAHRENHEIT -> {
                windStringResId = R.string.weather_detail_wind_direction_and_speed_fahrenheit
                temperatureSymbolResId = R.string.common_temperature_fahrenheit_symbol
            }
            WeatherBitUnitEnum.SCIENTIFIC -> {
                windStringResId = R.string.weather_detail_wind_direction_and_speed_scientific
                temperatureSymbolResId = R.string.common_temperature_kelvin_symbol
            }
        }

        onView(withId(R.id.textViewTemperatureMinSymbol)).check(
            matches(
                withText(
                    targetContext.getString(
                        temperatureSymbolResId
                    )
                )
            )
        )
        onView(withId(R.id.textViewTemperatureMaxSymbol)).check(
            matches(
                withText(
                    targetContext.getString(
                        temperatureSymbolResId
                    )
                )
            )
        )
        onView(withId(R.id.textViewRelativeDay)).check(matches(withText(item?.relativeDay)))
        onView(withId(R.id.textViewDaySubtitle)).check(
            matches(
                withText(
                    targetContext.getString(
                        R.string.weather_detail_subtitle,
                        item?.dayOfWeek, item?.formattedDateTime
                    )
                )
            )
        )
        onView(withId(R.id.textViewTemperatureMin)).check(
            matches(
                withText(
                    DoubleUtil.removeTrailingZeros(
                        item?.minTemp
                    )
                )
            )
        )
        onView(withId(R.id.textViewTemperatureMax)).check(
            matches(
                withText(
                    DoubleUtil.removeTrailingZeros(
                        item?.maxTemp
                    )
                )
            )
        )
        onView(
            withIndex(
                withId(R.id.textViewWeatherDescription),
                0
            )
        ).check(matches(withText(item?.weather?.description)))
        onView(withId(R.id.textViewRainChancePercent)).check(
            matches(
                withText(
                    targetContext.getString(
                        R.string.weather_detail_rain_chance,
                        item?.precipitationChancePercent
                    )
                )
            )
        )
        onView(withId(R.id.textViewCloudCoverPercent)).check(
            matches(
                withText(
                    targetContext.getString(
                        R.string.weather_detail_cloud_cover,
                        item?.cloudCoverPercentage
                    )
                )
            )
        )
        onView(withId(R.id.textViewWindDirectionAndSpeed)).check(
            matches(
                withText(
                    targetContext.getString(
                        windStringResId,
                        item?.windDirectionDescription,
                        DoubleUtil.removeTrailingZeros(item?.windSpeed)
                    )
                )
            )
        )
    }

    private fun withIndex(
        matcher: Matcher<View?>,
        index: Int
    ): Matcher<View?>? {
        return object : TypeSafeMatcher<View?>() {
            var currentIndex = 0
            override fun describeTo(description: Description) {
                description.appendText("with index: ")
                description.appendValue(index)
                matcher.describeTo(description)
            }

            override fun matchesSafely(view: View?): Boolean {
                return matcher.matches(view) && currentIndex++ == index
            }
        }
    }
}
