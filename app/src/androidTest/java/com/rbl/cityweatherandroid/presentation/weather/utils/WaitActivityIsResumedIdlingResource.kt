package com.rbl.cityweatherandroid.presentation.weather.utils

import androidx.test.espresso.IdlingResource
import androidx.test.espresso.IdlingResource.ResourceCallback
import androidx.test.runner.lifecycle.ActivityLifecycleMonitor
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage

class WaitActivityIsResumedIdlingResource(activityToWaitClassName: String) :
    IdlingResource {
    private val instance: ActivityLifecycleMonitor
    private val activityToWaitClassName: String

    @Volatile
    private var resourceCallback: ResourceCallback? = null
    private var resumed = false
    override fun getName(): String {
        return this.javaClass.name
    }

    override fun isIdleNow(): Boolean {
        resumed = isActivityLaunched
        if (resumed && resourceCallback != null) {
            resourceCallback!!.onTransitionToIdle()
        }
        return resumed
    }

    private val isActivityLaunched: Boolean
        private get() {
            val activitiesInStage =
                instance.getActivitiesInStage(Stage.RESUMED)
            for (activity in activitiesInStage) {
                if (activity.javaClass.name == activityToWaitClassName) {
                    return true
                }
            }
            return false
        }

    override fun registerIdleTransitionCallback(resourceCallback: ResourceCallback) {
        this.resourceCallback = resourceCallback
    }

    init {
        instance = ActivityLifecycleMonitorRegistry.getInstance()
        this.activityToWaitClassName = activityToWaitClassName
    }
}