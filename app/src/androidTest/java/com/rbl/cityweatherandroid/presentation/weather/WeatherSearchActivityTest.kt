package com.rbl.cityweatherandroid.presentation.weather


import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.rbl.cityweatherandroid.R
import com.rbl.cityweatherandroid.data.model.Forecast
import com.rbl.cityweatherandroid.presentation.weather.utils.WaitActivityIsResumedIdlingResource
import com.rbl.cityweatherandroid.presentation.weather_detail.WeatherDetailActivity
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*
import org.hamcrest.TypeSafeMatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class WeatherSearchActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(WeatherSearchActivity::class.java)

    private lateinit var instrumentationContext: Context

    @Before
    fun setup() {
        instrumentationContext = InstrumentationRegistry.getInstrumentation().context
        Intents.init();
    }

    @After
    fun tearDown() {
        Intents.release();
    }

    @Test
    fun checkIfDetailActivityIsCalledAfterCityIsPicked() {
        // Click on auto complete text view
        val appCompatAutoCompleteTextView = onView(
            allOf(
                withId(R.id.textViewCity),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.textField),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        // Fill in half of the city's name
        appCompatAutoCompleteTextView.perform(replaceText("curi"), closeSoftKeyboard())

        // Find view with the correct city name and click
        onView(withText("Curitiba (Brazil)")).inRoot(
            withDecorView(
                not(
                    `is`(
                        mActivityTestRule.activity.window.decorView
                    )
                )
            )
        )
            .perform(click());

        // Check if the loading animation is showing
        onView(withId(R.id.includeLoadingAnimation))
            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)));

        val className = WeatherDetailActivity.getStartIntent(
            instrumentationContext,
            Forecast("", "", null)
        ).component?.className

        // Wait for activity to start with  our intent
        className?.let {
            val idlingResource = WaitActivityIsResumedIdlingResource(className)
            try {
                IdlingRegistry.getInstance().register(idlingResource)

                // If intent was called will return success
                intended(
                    hasComponent(
                        className
                    )
                )
            } finally {
                IdlingRegistry.getInstance().unregister(idlingResource)
            }
        }
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
