# City Weather App

City Weather is an android app made for a job listing [test](https://bitbucket.org/delivery_center/android-test/src/master/) by [deliverycenter](deliverycenter.com) which provides a fast way for delivery people to check the weather for any city

## Key features
- Developed in MVVM architecture for easy mantainability
- Is internationalized (Currently only in english/portuguese)
- Has an easy and clean, easy to follow design 
- Has UI and Unit tests

## Contributions

Many thanks to **my brother** for making the app icon and the loading animation illustrations